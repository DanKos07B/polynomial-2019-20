import static org.junit.Assert.*;
import org.junit.Test;

public class ProductTest {
    @Test
    public void when_productEmpty_returnsOne() {
        Polynomial prod = Polynomial.product();
        Polynomial expected = new Polynomial(1);
        assertEquals(expected, prod);
    }

    @Test
    public void when_productHasOneArgument_returnsEquivalentPolynomial() {
        Polynomial source = new Polynomial(1, 2, 3);
        Polynomial prod = Polynomial.product(source);
        assertEquals(source, prod);
    }

    @Test
    public void withTwoArguments_productGivesCorrectResult(){
        Polynomial a = new Polynomial(1,2,3);
        Polynomial b = new Polynomial(4,5);
        Polynomial expected = new Polynomial(4,13,22,15);

        Polynomial prod = Polynomial.product(a,b);
        assertEquals(expected, prod);
    }

    @Test
    public void withThreeArguments_sumGivesCorrectResult(){
        Polynomial a = new Polynomial(1,2);
        Polynomial b = new Polynomial(-1,-2);
        Polynomial c = new Polynomial(3,3,3);
        Polynomial expected = new Polynomial(-3,-15,-27,-24,-12);

        Polynomial prod = Polynomial.product(a,b,c);
        assertEquals(expected, prod);
    }
}