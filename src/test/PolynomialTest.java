import static org.junit.Assert.*;

import org.junit.Test;

public class PolynomialTest {
    @Test
    public void getDegreeEmpty() {
        Polynomial a = new Polynomial();
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeCubic() {
        Polynomial a = new Polynomial(7, 8, 9, 10);
        assertEquals(3, a.getDegree());
    }

    @Test
    public void getCoefficientQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(7, a.getCoefficient(0));
        assertEquals(8, a.getCoefficient(1));
        assertEquals(9, a.getCoefficient(2));
    }

    @Test
    public void getCoefficientOutOfRange() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(0, a.getCoefficient(3));
        assertEquals(0, a.getCoefficient(4));
    }

    @Test
    public void toStringNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals("Polynomial[42]", a.toString());
    }

    @Test
    public void toStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("Polynomial[7,8,9]", a.toString());
    }

    @Test
    public void toPrettyStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9x^2 + 8x + 7", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringQuadraticWithY() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9y^2 + 8y + 7", a.toPrettyString("y"));
    }

    @Test
    public void toPrettyStringWithOne() {
        Polynomial a = new Polynomial(1, 2, 1);
        assertEquals("x^2 + 2x + 1", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithZero() {
        Polynomial a = new Polynomial(3, 0, 5);
        assertEquals("5x^2 + 3", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithNegative() {
        Polynomial a = new Polynomial(17, 0, -2, -5);
        assertEquals("-5x^3 - 2x^2 + 17", a.toPrettyString("x"));
    }

    @Test
    public void comparePositive() {
        Polynomial a = new Polynomial(1, 2, 3);
        Polynomial b = new Polynomial(1, 2, 3, 0);
        assertEquals(a, b);
    }

    @Test
    public void compareNegative() {
        Polynomial a = new Polynomial(1, 2, 0, 3);
        Polynomial b = new Polynomial(1, 0, 2, 3);
        assertNotEquals(a, b);
    }
}
