import static org.junit.Assert.*;
import org.junit.Test;

public class SumTest {
    @Test
    public void when_sumEmpty_returnsZeroPolynomial() {
        Polynomial sum = Polynomial.sum();
        Polynomial expected = new Polynomial(0);
        assertEquals(expected, sum);
    }

    @Test
    public void when_sumHasOneArgument_returnsEquivalentPolynomial() {
        Polynomial source = new Polynomial(1, 2, 3);
        Polynomial sum = Polynomial.sum(source);
        assertEquals(source, sum);
    }

    @Test
    public void withTwoArguments_sumGivesCorrectResult(){
        Polynomial a = new Polynomial(1,2,3,4);
        Polynomial b = new Polynomial(4,5,6);
        Polynomial expected = new Polynomial(5,7,9,4);

        Polynomial sum = Polynomial.sum(a,b);
        assertEquals(expected, sum);
    }

    @Test
    public void withThreeArguments_sumGivesCorrectResult(){
        Polynomial a = new Polynomial(1,2,3,4);
        Polynomial b = new Polynomial(4,5,6);
        Polynomial c = new Polynomial(-6,-6,-6,-6,-6);
        Polynomial expected = new Polynomial(-1,1,3,-2,-6);

        Polynomial sum = Polynomial.sum(a,b,c);
        assertEquals(expected, sum);
    }
}
