class DivResult {
    public DivResult(Polynomial result, Polynomial remainder) {
        this.result = result;
        this.remainder = remainder;
    }

    Polynomial result;
    Polynomial remainder;
}

/**
 * Integer-only polynomials.
 */
public class Polynomial {

    private int[] value;


    /**
     * Create new instance with given coefficients.
     * <p>
     * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1)
     * x^5 + 42 would be created by new Polynomial(42, 0, 0, 0, 0, 1)
     *
     * @param coef Coefficients, ordered from lowest degree (power).
     */
    public Polynomial(int... coef) {
        value = coef.clone();
    }

    //copy constructor
    public Polynomial(Polynomial p) {
        value = p.value.clone();
    }

    public static Polynomial BuildSingleTermPolynomial(int degree, int coef) {
        int[] coefs = new int[degree + 1];
        coefs[degree] = coef;
        return new Polynomial(coefs);
    }

    /**
     * Get coefficient value for given degree (power).
     */
    public int getCoefficient(int deg) {
        if (deg >= value.length) {
            return 0;
        } else {
            return value[deg];
        }
    }

    /**
     * Get degree (max used power) of the polynomial.
     */
    public int getDegree() {
        for (int i = value.length - 1; i >= 0; i--) {
            if (value[i] != 0) {
                return i;
            }
        }
        return 0;
    }

    /**
     * Format into human-readable form with given variable.
     */
    public String toPrettyString(String variable) {
        StringBuilder sb = new StringBuilder();
        int degree = getDegree();
        int coef = getCoefficient(degree);

        if (degree == 0) {
            sb.append(coef);
        } else {  //degree > 0
            if (coef == -1) sb.append('-');
            else if (coef != 1) sb.append(coef);

            sb.append(variable);
            if (degree > 1) {
                sb.append('^');
                sb.append(degree);
            }
        }

        for (int i = degree - 1; i >= 0; i--) {
            coef = getCoefficient(i);

            if (coef == 0) continue;
            if (coef < 0) {
                sb.append(" - ");
                coef = -coef;
            } else {
                sb.append(" + ");
            }

            if (i == 0) {
                sb.append(coef);
            } else {  //i>0
                if (coef != 1) sb.append(coef);
                sb.append(variable);
                if (i > 1) {
                    sb.append('^');
                    sb.append(i);
                }
            }
        }
        return sb.toString();
    }


    /**
     * Debugging output, dump only coefficients.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Polynomial[");
        for (int i = 0; i < value.length; i++) {
            if (i > 0) sb.append(',');
            sb.append(value[i]);
        }
        sb.append(']');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Polynomial)) {
            return false;
        }

        Polynomial p = (Polynomial) o;

        int degree = p.getDegree();
        if (this.getDegree() != degree) return false;

        for (int i = 0; i < degree; i++) {
            if (p.getCoefficient(i) != this.getCoefficient(i)) return false;
        }

        return true;
    }

    private static int findMaxDegree(Polynomial... polynomials) {
        int maxDegree = polynomials[0].getDegree();
        int tempDegree;
        for (int i = 1; i < polynomials.length; i++) {
            tempDegree = polynomials[i].getDegree();
            if (tempDegree > maxDegree) maxDegree = tempDegree;
        }
        return maxDegree;
    }

    /**
     * Adds together given polynomials, returning new one.
     */
    public static Polynomial sum(Polynomial... polynomials) {
        if (polynomials.length < 1) return new Polynomial(0);
        if (polynomials.length == 1) return new Polynomial(polynomials[0]);

        int maxDegree = findMaxDegree(polynomials);

        int[] resultCoefs = new int[maxDegree + 1];

        for (int i = 0; i <= maxDegree; i++) {
            resultCoefs[i] = 0;
            for (Polynomial p : polynomials) {
                resultCoefs[i] += p.getCoefficient(i);
            }
        }

        return new Polynomial(resultCoefs);
    }

    /**
     * Multiplies together given polynomials, returning new one.
     */
    public static Polynomial product(Polynomial... polynomials) {
        if (polynomials.length < 1) return new Polynomial(1);
        if (polynomials.length == 1) return new Polynomial(polynomials[0]);

        Polynomial product = polynomials[0];

        for (int i = 1; i < polynomials.length; i++) {
            product = binaryProduct(product, polynomials[i]);
        }

        return product;
    }

    private static Polynomial binaryProduct(Polynomial a, Polynomial b) {
        int degreeA = a.getDegree();
        int degreeB = b.getDegree();
        int maxResultDegree = degreeA + degreeB;

        int[] resultCoefs = new int[maxResultDegree + 1];

        int deg, coef;
        for (int i = 0; i <= degreeA; i++) {
            for (int j = 0; j <= degreeB; j++) {
                deg = i + j;
                coef = a.getCoefficient(i) * b.getCoefficient(j);
                resultCoefs[deg] += coef;
            }
        }

        return new Polynomial(resultCoefs);
    }



    private static DivResult division(Polynomial numerator, Polynomial denominator) {
        Polynomial result = new Polynomial(0);

        while (true) {
            int numDegree = numerator.getDegree();
            int denDegree = denominator.getDegree();
            int numTopCoef = numerator.getCoefficient(numDegree);
            int denTopCoef = denominator.getCoefficient(denDegree);

            int newDegree = numDegree - denDegree;
            int newCoef = numTopCoef / denTopCoef;

            if (newDegree < 0 || newCoef == 0) break;
            else {
                Polynomial number = BuildSingleTermPolynomial(newDegree, newCoef);
                Polynomial tosubtract = product(number, denominator, new Polynomial(-1));
                result = sum(number, result);
                numerator = sum(tosubtract, numerator);
            }
        }

        return new DivResult(result, numerator);
    }


    /**
     * Get the result of division of two polynomials, ignoring remainder.
     */
    public static Polynomial div(Polynomial dividend, Polynomial divisor) {
        return division(dividend, divisor).result;
    }

    /**
     * Get the remainder of division of two polynomials.
     */
    public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
        return division(dividend, divisor).remainder;
    }
}