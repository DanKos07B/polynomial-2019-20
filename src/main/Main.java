
public class Main {
    public static void main(String[] args) {
        if (args.length < 4) {
            printHelp();
            return;
        }

        String op = args[0];

        int delimPosition = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals("--")) {
                delimPosition = i;
                break;
            }
        }
        if (delimPosition == 0) {
            printHelp();
            return;
        }

        int[] coefFirst = parseCoefficients(args, 1, delimPosition);
        int[] coefSecond = parseCoefficients(args, delimPosition + 1, args.length);

        if ((coefFirst.length == 0) || (coefSecond.length == 0)) {
            printHelp();
            return;
        }

        Polynomial first = new Polynomial(coefFirst);
        Polynomial second = new Polynomial(coefSecond);

        Polynomial result;
        Polynomial remainder = new Polynomial(0);
        String opSymbol;
        if (op.equals("sum")) {
            result = Polynomial.sum(first, second);
            opSymbol = "+";
        } else if (op.equals("product")) {
            result = Polynomial.product(first, second);
            opSymbol = "*";
        } else if (op.equals("div")) {
            result = Polynomial.div(first, second);
            remainder = Polynomial.mod(first,second);
            opSymbol = "/";
        } else if (op.equals("mod")) {
            result = Polynomial.mod(first,second);
            opSymbol = "%";
        } else {
            printHelp();
            return;
        }

        System.out.printf("(%s) %s (%s) = (%s)",
                first.toPrettyString("x"),
                opSymbol,
                second.toPrettyString("x"),
                result.toPrettyString("x"));
        if(op.equals("div")) System.out.printf(", with the remainder (%s)", remainder.toPrettyString("x"));
        System.out.println();
    }

    private static void printHelp() {
        System.out.println("Usage:");
        System.out.println("> polynomial OP a1 [a2 ... aM] -- b1 [b2 ... bN]");
        System.out.println("The first argument is the operator ('sum', 'product', 'mod' or 'div').");
        System.out.println("Next is the first polynomial, for example '1 0 3' denoting 'x^2 + 3'.");
        System.out.println("The second polynomial is separated from the first one by the delimiter '--'.");
    }

    //output array is in reverse order
    private static int[] parseCoefficients(String[] args, int firstIndexIncl, int lastIndexExcl) {
        int[] result = new int[lastIndexExcl - firstIndexIncl];
        int resIndex = result.length - 1;
        for (int i = firstIndexIncl; i < lastIndexExcl; i++) {
            result[resIndex] = Integer.parseInt(args[i]);
            resIndex--;
        }
        return result;
    }

}
