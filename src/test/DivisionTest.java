import static org.junit.Assert.*;
import org.junit.Test;

public class DivisionTest {
    @Test
    public void divWithoutRemainderTest(){
        Polynomial num = new Polynomial(-10,-3,1);
        Polynomial den = new Polynomial(2,1);
        Polynomial expected = new Polynomial(-5,1);

        Polynomial prod = Polynomial.div(num,den);
        assertEquals(expected, prod);
    }

    @Test
    public void modWithoutRemainderTest(){
        Polynomial num = new Polynomial(-10,-3,1);
        Polynomial den = new Polynomial(2,1);
        Polynomial expected = new Polynomial(0);

        Polynomial prod = Polynomial.mod(num,den);
        assertEquals(expected, prod);
    }

    @Test
    public void divWithRemainderTest(){
        Polynomial num = new Polynomial(-1,-5,2);
        Polynomial den = new Polynomial(-3,1);
        Polynomial expected = new Polynomial(1,2);

        Polynomial prod = Polynomial.div(num,den);
        assertEquals(expected, prod);
    }

    @Test
    public void modWithRemainderTest(){
        Polynomial num = new Polynomial(-1,-5,2);
        Polynomial den = new Polynomial(-3,1);
        Polynomial expected = new Polynomial(2);

        Polynomial prod = Polynomial.mod(num,den);
        assertEquals(expected, prod);
    }
}
